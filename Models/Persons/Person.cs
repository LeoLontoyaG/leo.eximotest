﻿using Leo.PruebaTecnicaEximo.Models.Companies;
using Leo.PruebaTecnicaEximo.Models.Forms;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace Leo.PruebaTecnicaEximo.Models.Persons
{
    [Table("Persons")]

    public class Person : BaseEntity
    {
        [Required]
        [StringLength(32)]
        public string Name { get; set; }

        [StringLength(64)]
        public string Email { get; set; }

        [StringLength(64)]
        public string Phone { get; set; }

        public Guid CompanyId { get; set; }

        [JsonIgnore]
        public Company? Company { get; set; }

        [JsonIgnore]
        public List<Form>? Forms { get; set;} 

    }
}
