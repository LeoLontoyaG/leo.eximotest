﻿using Leo.PruebaTecnicaEximo.Models.Forms;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace Leo.PruebaTecnicaEximo.Models.FormTypes
{
    [Table("FormTypes")]
    public class FormType: BaseEntity
    {
        [StringLength(64)]
        public string Description { get; set; }


        [JsonIgnore]
        public List<Form>? FormXFormtype { get; set; }
    }
}
