﻿using Leo.PruebaTecnicaEximo.Models.Companies;
using Leo.PruebaTecnicaEximo.Models.Forms;
using Leo.PruebaTecnicaEximo.Models.FormTypes;
using Leo.PruebaTecnicaEximo.Models.Persons;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;

namespace Leo.PruebaTecnicaEximo.Models
{
    public class AppDbContext : DbContext
    {
        protected readonly IConfiguration configuration;

        public AppDbContext(IConfiguration configuration)
        {
            this.configuration = configuration;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseNpgsql(configuration.GetConnectionString("WebApiDatabase"));

            base.OnConfiguring(optionsBuilder);
        }
        public DbSet<Company> Companies { get; set; }

        public DbSet<Form> Forms { get; set; }

        public DbSet<Person> Persons { get; set; }

        public DbSet<FormType> FormTypes { get; set; }

    }
}