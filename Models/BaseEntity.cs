﻿using System.ComponentModel.DataAnnotations;

namespace Leo.PruebaTecnicaEximo.Models
{
    public class BaseEntity
    {
        public Guid Id { get; set; } = Guid.NewGuid();

        
        public DateTime? ModificationDate { get; set; }

        public DateTime CreationDate { get; set; }= DateTime.Now;

        public Boolean IsDeleted { get; set; } = false;
    }
}
