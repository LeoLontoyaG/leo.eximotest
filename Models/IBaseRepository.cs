﻿using System.Linq.Expressions;

namespace Leo.PruebaTecnicaEximo.Models
{
    public interface IBaseRepository<T> : IDisposable where T : BaseEntity, new()
    {
        BaseResponse<T> GetAll();

        BaseResponse<int> InsertAndSave(T entity);

        BaseResponse<T> GetById(Guid id);
        int Save();
        void Insert(T entity);

        BaseResponse<int> Count(Expression<Func<T, bool>> predicate);

        void DeleteById(Guid id);

        BaseResponse<int> DeleteByIdAndSave(Guid id);

    }
}
