﻿using Leo.PruebaTecnicaEximo.Models.Persons;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace Leo.PruebaTecnicaEximo.Models.Companies
{
    [Table("Companies")]

    public class Company: BaseEntity
    {
        [Required]
        [StringLength(32)]
        public string Name { get; set; }

        [Required]
        [StringLength(32)]
        public string Address { get; set; }

        [JsonIgnore]
        public List<Person>? PersonXCompany { get; set; }

    }
}
