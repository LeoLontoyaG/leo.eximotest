﻿using System.Linq.Expressions;
using System;

namespace Leo.PruebaTecnicaEximo.Models
{
    public class BaseRepository<T> : IBaseRepository<T> where T : BaseEntity, new()
    {
        public readonly AppDbContext Context;
        private bool _disposed = false;
        public BaseRepository(AppDbContext context)
        {
            Context = context;
        }

        public virtual void Dispose(bool disposing)
        {
            if (!this._disposed)
            {
                if (disposing)
                {
                    Context.Dispose();
                }
            }
            this._disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public BaseResponse<T> GetAll()
        {
            var results = Context.Set<T>().ToList();
            return new BaseResponse<T>(StatusCodes.Status200OK, results);
        }

        public void Insert(T entity)
        {
            Context.Set<T>().Add(entity);
        }

        public int Save()
        {
            return Context.SaveChanges();
        }

        public BaseResponse<int> InsertAndSave(T entity)
        {
            Insert(entity);
            var result = Save();
            return new BaseResponse<int>(StatusCodes.Status200OK, result);
        }

        public BaseResponse<int> Count(Expression<Func<T, bool>> predicate)
        {
            var where = predicate ?? (e => true);
            var result = Context.Set<T>().Count(where);
            return new BaseResponse<int>(StatusCodes.Status200OK, result);
        }


        public BaseResponse<T> GetById(Guid id)
        {
            var result = Context.Set<T>().SingleOrDefault(e => e.Id.Equals(id));
            return new BaseResponse<T>(StatusCodes.Status200OK, result);
        }

        public void DeleteById(Guid id)
        {
            var response = GetById(id);
            if (response.StatusCode != 200)
            {
                return;
            }
            Context.Set<T>().Remove(response.Result);
        }

        public BaseResponse<int> DeleteByIdAndSave(Guid id)
        {
            DeleteById(id);
            var result = Save();
            return new BaseResponse<int>(StatusCodes.Status200OK, result);
        }

    }
}
