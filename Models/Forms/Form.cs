﻿using Leo.PruebaTecnicaEximo.Models.FormTypes;
using Leo.PruebaTecnicaEximo.Models.Persons;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace Leo.PruebaTecnicaEximo.Models.Forms
{
    [Table("Forms")]

    public class Form: BaseEntity
    {
        [Required]
        [StringLength(32)]
        public string Name { get; set; }

        [Required]
        [StringLength(256)]
        public string Description { get; set; }

        [Required]
        public Guid PersonId { get; set; }

        [JsonIgnore]
        public Person? Person { get; set; }

           
        public Guid FormTypeId { get; set; }
        [JsonIgnore]
        public FormType? FormType { get; set; }

        [Required]
        [Range(1, 5)]
        public int Priority { get; set; }


        [Required]
        public Boolean Active { get; set; }
    }
}
