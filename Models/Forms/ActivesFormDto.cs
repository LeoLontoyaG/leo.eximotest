﻿using System.ComponentModel.DataAnnotations;

namespace Leo.PruebaTecnicaEximo.Models.Forms
{
    public class ActivesFormDto: Form
    {
        [StringLength(64)]
        public string FormTypeDescription { get; set; }

        [StringLength(32)]
        public string PersonName { get; set; }

    }
}
