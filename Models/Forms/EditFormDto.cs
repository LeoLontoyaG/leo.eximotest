﻿using System.ComponentModel.DataAnnotations;

namespace Leo.PruebaTecnicaEximo.Models.Forms
{
    public class EditFormDto:Form
    {

        public Guid CompanyId { get; set; }

        public string FormTypeDescription { get; set;}

    }
}
