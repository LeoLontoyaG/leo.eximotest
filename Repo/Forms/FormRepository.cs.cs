﻿using Leo.PruebaTecnicaEximo.Models;
using Leo.PruebaTecnicaEximo.Models.Forms;
using Leo.PruebaTecnicaEximo.Models.Persons;
using System.Text.RegularExpressions;

namespace Leo.PruebaTecnicaEximo.Repo.Forms
{
    public class FormRepository: BaseRepository<Form>,IFormRepository
    {
        public readonly AppDbContext _Context;
        public FormRepository(
             AppDbContext context

            ) : base(context)
        {
            _Context = context;
        }

        public BaseResponse<ActivesFormDto> GetAllActivos()
        {
            try
            {

                var res = (from frms in _Context.Forms
                           join frmstpes in _Context.FormTypes on frms.FormTypeId equals frmstpes.Id
                           join per in _Context.Persons on frms.PersonId equals per.Id
                           where frms.Active == true && frms.IsDeleted == false
                           select new ActivesFormDto
                           {
                               Id = frms.Id,
                               Name = frms.Name,
                               Description = frms.Description,
                               PersonName = per.Name,
                               PersonId = per.Id,
                               Priority= frms.Priority,
                               FormTypeDescription = frmstpes.Description
                           }
                         ).ToList();



                return new BaseResponse<ActivesFormDto>(StatusCodes.Status200OK, res);
            }

            catch(Exception e) {
                return new BaseResponse<ActivesFormDto>(StatusCodes.Status500InternalServerError, e.Message, e.StackTrace);

            }
        }

        public BaseResponse<EditFormDto> getformbyid(Guid id)
        {
            try
            {
                var formulario = (from frms in _Context.Forms
                                  join per in _Context.Persons on frms.PersonId equals per.Id
                                  join f in _Context.FormTypes on frms.FormTypeId equals f.Id
                                  where frms.Id == id && frms.Active == true && frms.IsDeleted == false
                                  select new EditFormDto
                                  {
                                      Id = frms.Id,
                                      Name = frms.Name,
                                      Description = frms.Description,
                                      CompanyId = per.CompanyId,
                                      FormTypeId = frms.FormTypeId,
                                      Priority = frms.Priority,
                                      CreationDate = frms.CreationDate,
                                      PersonId = per.Id,
                                      FormTypeDescription = f.Description,
                                  }).FirstOrDefault();

                if (formulario != null)
                {
                    return new BaseResponse<EditFormDto>(StatusCodes.Status200OK, formulario);
                }
                else
                {
                    return new BaseResponse<EditFormDto>(StatusCodes.Status404NotFound, "Formulario no encontrado");
                }
            }
            catch (Exception ex)
            {
                return new BaseResponse<EditFormDto>(StatusCodes.Status500InternalServerError, ex.Message);
            }

        }

        public BaseResponse<Form> EditForm(Form form)
        {
            try
            {
                string id=form.Id.ToString();
                id = id.Replace("{", "").Replace("}", "");
                var formulario = _Context.Forms.FirstOrDefault(f => f.Id == Guid.Parse(id));
                if (formulario != null)
                {
                    formulario.Name = form.Name;
                    formulario.Description = form.Description;
                    formulario.FormTypeId = form.FormTypeId;
                    formulario.PersonId= form.PersonId;
                    formulario.Priority = form.Priority;
                    formulario.ModificationDate=form.ModificationDate;
                    formulario.Active = true;

                    Context.Update(formulario);
                    Context.SaveChanges();
                }
                else
                {
                    return null;
                }
                return new BaseResponse<Form>(StatusCodes.Status200OK, formulario);

            }
            catch(Exception e)
            {
                return new BaseResponse<Form>(StatusCodes.Status500InternalServerError, e.Message, e.StackTrace);

            }
        }


        public BaseResponse<Form> deleteById(Guid id)
        {

            try
            {
                var formulario = _Context.Forms.FirstOrDefault(f => f.Id == id);

                if(formulario != null)
                {
                    formulario.Active= false;
                    _Context.Update(formulario);
                    _Context.SaveChanges();
                    return new BaseResponse<Form>(StatusCodes.Status200OK, formulario);
                }
                else
                {
                    return null;
                }

            }
            catch (Exception e)
            {
                return new BaseResponse<Form>(StatusCodes.Status500InternalServerError, e.Message, e.StackTrace);
            }
        }

    }
}
