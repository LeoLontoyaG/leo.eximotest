﻿using Leo.PruebaTecnicaEximo.Models;
using Leo.PruebaTecnicaEximo.Models.Forms;

namespace Leo.PruebaTecnicaEximo.Repo.Forms
{
    public interface IFormRepository:IBaseRepository<Form>
    {
        public BaseResponse<ActivesFormDto> GetAllActivos();
        public BaseResponse<Form> deleteById(Guid id);

        public BaseResponse<EditFormDto> getformbyid(Guid id);

        public BaseResponse<Form> EditForm(Form form);
    }
}
