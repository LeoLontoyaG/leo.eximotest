﻿using Leo.PruebaTecnicaEximo.Models;
using Leo.PruebaTecnicaEximo.Models.Companies;

namespace Leo.PruebaTecnicaEximo.Repo.Companies
{
    public interface ICompanyRepository: IBaseRepository<Company>
    {
    }
}
