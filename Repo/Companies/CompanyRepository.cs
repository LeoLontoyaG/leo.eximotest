﻿using Leo.PruebaTecnicaEximo.Models;
using Leo.PruebaTecnicaEximo.Models.Companies;

namespace Leo.PruebaTecnicaEximo.Repo.Companies
{
    public class CompanyRepository : BaseRepository<Company>, ICompanyRepository
    {
        public readonly AppDbContext _Context;
        public CompanyRepository(
             AppDbContext context

            ) : base(context)
        {
            _Context = context;
        }
    }
}
