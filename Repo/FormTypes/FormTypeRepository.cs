﻿using Leo.PruebaTecnicaEximo.Models;
using Leo.PruebaTecnicaEximo.Models.FormTypes;

namespace Leo.PruebaTecnicaEximo.Repo.FormTypes
{
    public class FormTypeRepository: BaseRepository<FormType>,IFormTypeRepository
    {
        public readonly AppDbContext _Context;
        public FormTypeRepository(
             AppDbContext context

            ) : base(context)
        {
            _Context = context;
        }


        public BaseResponse<FormType> getAllTypes()
        {
            try
            {
                var entities = _Context.FormTypes.Where(f =>  f.IsDeleted == false).ToList();
                return new BaseResponse<FormType>(StatusCodes.Status200OK, entities);
            }

            catch (Exception e)
            {
                return new BaseResponse<FormType>(StatusCodes.Status500InternalServerError, e.Message, e.StackTrace);

            }
        }
    }
}
