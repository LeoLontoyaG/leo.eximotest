﻿using Leo.PruebaTecnicaEximo.Models;
using Leo.PruebaTecnicaEximo.Models.FormTypes;

namespace Leo.PruebaTecnicaEximo.Repo.FormTypes
{
    public interface IFormTypeRepository: IBaseRepository<FormType>
    {
        public BaseResponse<FormType> getAllTypes();
    }
}
