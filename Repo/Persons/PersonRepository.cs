﻿using Leo.PruebaTecnicaEximo.Models;
using Leo.PruebaTecnicaEximo.Models.FormTypes;
using Leo.PruebaTecnicaEximo.Models.Persons;

namespace Leo.PruebaTecnicaEximo.Repo.Persons
{
    public class PersonRepository: BaseRepository<Person>, IPersonRepository
    {
        public readonly AppDbContext _Context;
        public PersonRepository(
             AppDbContext context

            ) : base(context)
        {
            _Context = context;
        }

        public BaseResponse<Person> getEmpleadosByEmpresaid(Guid id)
        {
            try
            {
                var entities = _Context.Persons.Where(f => f.IsDeleted == false && f.CompanyId==id).ToList();
                return new BaseResponse<Person>(StatusCodes.Status200OK, entities);
            }

            catch (Exception e)
            {
                return new BaseResponse<Person>(StatusCodes.Status500InternalServerError, e.Message, e.StackTrace);

            }
        }


    }
}
