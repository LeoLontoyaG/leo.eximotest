﻿using Leo.PruebaTecnicaEximo.Models;
using Leo.PruebaTecnicaEximo.Models.Persons;

namespace Leo.PruebaTecnicaEximo.Repo.Persons
{
    public interface IPersonRepository: IBaseRepository<Person>
    {
        public BaseResponse<Person> getEmpleadosByEmpresaid(Guid id);
    }
}
