using Leo.PruebaTecnicaEximo.Models;
using Leo.PruebaTecnicaEximo.Models.Companies;
using Leo.PruebaTecnicaEximo.Models.Forms;
using Leo.PruebaTecnicaEximo.Models.FormTypes;
using Leo.PruebaTecnicaEximo.Models.Persons;
using Leo.PruebaTecnicaEximo.Repo.Companies;
using Leo.PruebaTecnicaEximo.Repo.Forms;
using Leo.PruebaTecnicaEximo.Repo.FormTypes;
using Leo.PruebaTecnicaEximo.Repo.Persons;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddDbContext<AppDbContext>();


builder.Services.AddScoped<IBaseRepository<Company>, BaseRepository<Company>>();
builder.Services.AddScoped<ICompanyRepository, CompanyRepository>();


builder.Services.AddScoped<IBaseRepository<Person>, BaseRepository<Person>>();
builder.Services.AddScoped<IPersonRepository, PersonRepository>();

builder.Services.AddScoped<IBaseRepository<Form>, BaseRepository<Form>>();
builder.Services.AddScoped<IFormRepository, FormRepository>();

builder.Services.AddScoped<IBaseRepository<FormType>, BaseRepository<FormType>>();
builder.Services.AddScoped<IFormTypeRepository, FormTypeRepository>();


builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.AddCors(options =>
{
    options.AddPolicy("AllowOrigin",
        builder => builder.WithOrigins("http://localhost:4200", "https://leoeximotest.netlify.app") // Agrega aqu� el origen de tu aplicaci�n Angular
                          .AllowAnyMethod()
                          .AllowAnyHeader());
});




var app = builder.Build();

// Configure the HTTP request pipeline.

app.UseSwagger();
app.UseSwaggerUI();


app.UseHttpsRedirection();

app.UseCors("AllowOrigin");

app.UseAuthorization();

app.MapControllers();

app.Run();
