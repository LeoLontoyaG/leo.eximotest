﻿using Leo.PruebaTecnicaEximo.Models.Forms;
using Leo.PruebaTecnicaEximo.Models;
using Microsoft.AspNetCore.Mvc;
using Leo.PruebaTecnicaEximo.Models.Companies;

namespace Leo.PruebaTecnicaEximo.Controllers
{
    [ApiController]
    [Route("api/Companies")]
    public class CompaniesController:BaseController<Company>
    {

        public readonly IBaseRepository<Company> _repository;
        private readonly ILogger<CompaniesController> _logger;
        private readonly IConfiguration _configuration;

        public CompaniesController
            (ILogger<CompaniesController> logger,
            IBaseRepository<Company> repository,
            IConfiguration configuration) : base(repository, logger)
        {
            _repository = repository;
            _logger = logger;
            _configuration = configuration;
        }


    }
}
