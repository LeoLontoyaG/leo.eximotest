﻿using Leo.PruebaTecnicaEximo.Models;
using Leo.PruebaTecnicaEximo.Models.Forms;
using Leo.PruebaTecnicaEximo.Repo.Forms;
using Microsoft.AspNetCore.Mvc;

namespace Leo.PruebaTecnicaEximo.Controllers
{
    [ApiController]
    [Route("api/Forms")]
    public class FormsController: BaseController<Form>
    {

        public readonly IBaseRepository<Form> _repository;
        private readonly ILogger<FormsController> _logger;
        private readonly IConfiguration _configuration;
        private readonly IFormRepository _formRepository;

        public FormsController
            (ILogger<FormsController> logger,
            IBaseRepository<Form> repository,
            IConfiguration configuration,
            IFormRepository formRepository) : base(repository, logger)
        {
            _repository = repository;
            _logger = logger;
            _configuration = configuration;
            _formRepository = formRepository;
        }

        [HttpGet, Route("getAllActivos")]
        public IActionResult Getexample()
        {
            try
            {
                var res=_formRepository.GetAllActivos();
                return Ok(res);
            }
            catch
            {
                return BadRequest();
            }
        }

        [HttpGet,Route("getformbyid")]

        public IActionResult getformbyid(string id)
        {
            try 
            {
                var res = _formRepository.getformbyid(Guid.Parse(id));
                if (res == null)
                {
                    return BadRequest("form no encontrada");
                }
                return Ok(res);
            }
            catch
            {
                return BadRequest();
            }

        }


        [HttpPost,Route("EditForm")]
        public IActionResult EditForm(Form form)
        {
            try
            {
                var res = _formRepository.EditForm(form);

                if (res != null)
                {
                    return Ok(res);
                }
                else { return BadRequest(); }
            }
            catch { 
                return BadRequest();
            }
        }


        [HttpDelete, Route("deleteById")]
        public IActionResult deleteById(string id)
        {
            try
            {
                var res =_formRepository.deleteById(Guid.Parse(id));
                if (res == null)
                {
                    return BadRequest("form no encontrada");
                }
                return Ok(res);
            }
            catch
            {
                return BadRequest();
            }
        }





    }
}
