﻿using Leo.PruebaTecnicaEximo.Models;
using Leo.PruebaTecnicaEximo.Models.FormTypes;
using Leo.PruebaTecnicaEximo.Repo.FormTypes;
using Microsoft.AspNetCore.Mvc;

namespace Leo.PruebaTecnicaEximo.Controllers
{
    [ApiController]
    [Route("api/FormTypes")]
    public class FormTypeController:BaseController<FormType>
    {

        public readonly IBaseRepository<FormType> _repository;
        private readonly ILogger<FormTypeController> _logger;
        private readonly IConfiguration _configuration;
        private readonly IFormTypeRepository _formTypeRepository;
        public FormTypeController
            (ILogger<FormTypeController> logger,
            IBaseRepository<FormType> repository,
            IConfiguration configuration,
            IFormTypeRepository formRepository) : base(repository, logger)
        {
            _repository = repository;
            _logger = logger;
            _configuration = configuration;
            _formTypeRepository = formRepository;
        }

        [HttpGet, Route("getAllTypes")]
        public IActionResult getAllTypes()
        {
            try
            {
                var res = _formTypeRepository.getAllTypes();
                return Ok(res);
            }
            catch(Exception e)
            {
                return BadRequest(e.Message);
            }
        }

    }
}
