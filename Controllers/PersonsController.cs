﻿using Leo.PruebaTecnicaEximo.Models;
using Leo.PruebaTecnicaEximo.Models.Persons;
using Leo.PruebaTecnicaEximo.Repo.Persons;
using Microsoft.AspNetCore.Mvc;

namespace Leo.PruebaTecnicaEximo.Controllers
{
    [ApiController]
    [Route("api/Persons")]
    public class PersonsController:BaseController<Person>
    {

        public readonly IBaseRepository<Person> _repository;
        private readonly ILogger<PersonsController> _logger;
        private readonly IConfiguration _configuration;
        private readonly IPersonRepository _personRepository;

        public PersonsController
            (ILogger<PersonsController> logger,
            IBaseRepository<Person> repository,
            IPersonRepository personRepository,
            IConfiguration configuration) : base(repository, logger)
        {
            _repository = repository;
            _logger = logger;
            _configuration = configuration;
            _personRepository = personRepository;
        }

        [HttpGet, Route("getEmpleadosByEmpresaid")]
        public IActionResult getEmpleadosByEmpresaid(string id)
        {
            try
            {
                var response =_personRepository.getEmpleadosByEmpresaid(Guid.Parse(id));
                return Ok(response);
            }
            catch(Exception e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}
