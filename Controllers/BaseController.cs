﻿using Leo.PruebaTecnicaEximo.Models;
using Microsoft.AspNetCore.Mvc;

namespace Leo.PruebaTecnicaEximo.Controllers
{
    public class BaseController<T> : ControllerBase where T : BaseEntity, new()
    {
        public readonly IBaseRepository<T> BaseRepository;
        public readonly ILogger<BaseController<T>> Logger;
        public readonly BaseControllerOptions BaseControllerOptions;

        protected BaseController(
            IBaseRepository<T> baseRepository,
            ILogger<BaseController<T>> logger,
            BaseControllerOptions baseControllerOptions = null)
        {
            BaseRepository = baseRepository;
            Logger = logger;
            if (baseControllerOptions == null)
            {
                BaseControllerOptions = new BaseControllerOptions();
            }
        }

        [HttpGet, Route("all")]
        public IActionResult GetAll()
        {
            BaseResponse<T> response = null;
            try
            {
                if (!BaseControllerOptions.GetAll)
                {
                    response = new BaseResponse<T>(StatusCodes.Status403Forbidden);
                    return StatusCode(StatusCodes.Status403Forbidden, response);
                }
                response = BaseRepository.GetAll();
                if (response.StatusCode != 200)
                    return BadRequest(response);
                return Ok(response);
            }
            catch (Exception e)
            {
                response = new BaseResponse<T>(StatusCodes.Status500InternalServerError, e.Message, e.StackTrace);
                return StatusCode(StatusCodes.Status500InternalServerError, response);
            }
        }

        [HttpPost, Route("add")]
        public IActionResult Add([FromBody] T value)
        {
            BaseResponse<int> response = null;
            try
            {
                if (!BaseControllerOptions.Add)
                {
                    response = new BaseResponse<int>(StatusCodes.Status403Forbidden);
                    return StatusCode(StatusCodes.Status403Forbidden, response);
                }
                response = BaseRepository.InsertAndSave(value);
                if (response.StatusCode != 200)
                    return BadRequest(response);
                return Ok(response);
            }
            catch (Exception e)
            {
                response = new BaseResponse<int>(StatusCodes.Status500InternalServerError, e.Message, e.StackTrace);
                return StatusCode(StatusCodes.Status500InternalServerError, response);
            }
        }


        [HttpGet, Route("count")]
        public IActionResult Count()
        {
            BaseResponse<int> response = null;
            try
            {
                if (!BaseControllerOptions.Count)
                {
                    response = new BaseResponse<int>(StatusCodes.Status403Forbidden);
                    return StatusCode(StatusCodes.Status403Forbidden, response);
                }
                response = BaseRepository.Count(null);
                if (response.StatusCode != 200)
                    return BadRequest(response);
                return Ok(response);
            }
            catch (Exception e)
            {
                response = new BaseResponse<int>(StatusCodes.Status500InternalServerError, e.Message, e.StackTrace);
                return StatusCode(StatusCodes.Status500InternalServerError, response);
            }
        }

        [HttpGet, Route("id/{id}")]
        public IActionResult GetById(Guid id)
        {
            BaseResponse<T> response = null;
            try
            {
                if (!BaseControllerOptions.GetById)
                {
                    response = new BaseResponse<T>(StatusCodes.Status403Forbidden);
                    return StatusCode(StatusCodes.Status403Forbidden, response);
                }
                response = BaseRepository.GetById(id);
                if (response.StatusCode != 200)
                    return BadRequest(response);
                return Ok(response);
            }
            catch (Exception e)
            {
                response = new BaseResponse<T>(StatusCodes.Status500InternalServerError, e.Message, e.StackTrace);
                return StatusCode(StatusCodes.Status500InternalServerError, response);
            }
        }


        [HttpDelete, Route("delete/{id}")]
        public IActionResult Delete(Guid id)
        {
            BaseResponse<int> response = null;
            try
            {
                if (!BaseControllerOptions.Delete)
                {
                    response = new BaseResponse<int>(StatusCodes.Status403Forbidden);
                    return StatusCode(StatusCodes.Status403Forbidden, response);
                }
                response = BaseRepository.DeleteByIdAndSave(id);
                if (response.StatusCode != 200)
                    return BadRequest(response);
                return Ok(response);
            }
            catch (Exception e)
            {
                response = new BaseResponse<int>(StatusCodes.Status500InternalServerError, e.Message, e.StackTrace);
                return StatusCode(StatusCodes.Status500InternalServerError, response);
            }
        }


    }


    public class BaseControllerOptions
    {
        public bool GetAll { get; set; } = true;
        public bool GetAllAsync { get; set; } = true;
        public bool GetById { get; set; } = true;
        public bool GetByIdAsync { get; set; } = true;
        public bool Count { get; set; } = true;
        public bool CountAsync { get; set; } = true;
        public bool Add { get; set; } = true;
        public bool AddAsync { get; set; } = true;
        public bool Edit { get; set; } = true;
        public bool EditAsync { get; set; } = true;
        public bool Delete { get; set; } = true;
        public bool DeleteAsync { get; set; } = true;
    }
}